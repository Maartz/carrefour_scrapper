defmodule Scrapper do
  @moduledoc"""
  Scrapper perform call on Carrefour.fr website to fetch data by EAN-13 code.
  It's based on HTTPoison and Floki.
  """
  @url "https://www.carrefour.fr/p/carrefour"

  def parse(product_code) do
    HTTPoison.start()
    url = get_url(product_code)
    {:ok, response} = HTTPoison.get("https://www.carrefour.fr/p/" <> url)
    content = get_html(response)
    title = get_tag(content, ".pdp-card__title")
    price = get_tag(content, ".product-card-price__price--final")
    {title, price}
  end

  defp get_url(ean) do
    HTTPoison.start()
    url =
      "#{@url}-#{ean}"
      |> HTTPoison.get!()
      |> get_html()
      |> get_title()
      |> List.to_string()
      |> split()
    url
  end

  defp get_html(response) do
    {:ok, html} = Floki.parse_document(response.body)
    html
  end

  defp get_title(url) do
    [{_, _, url}] = Floki.find(url, "title")
    url
  end

  defp get_tag(content, selector) do
    [{_, _, content}] = Floki.find(content, selector)
    content
      |> List.to_string()
      |> String.trim_leading()
      |> String.trim_trailing()
    end

  defp split(url) do
    [_, _, url] = String.split(url, " ")
    {_, url} = String.split_at(url, 3)
    url
  end

end
